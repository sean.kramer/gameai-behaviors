package graphs;
import java.util.ArrayList;
import java.util.List;

import processing.core.PVector;

public class Node implements Comparable<Node> {
	//Algorithm values
	public float f, h, g;
	public Node parent;
	
	//Index of node, used for efficient drawing of edges
	public int i;
	
	//Positional information in the graph
	public PVector p;
	public List<Node> edges = new ArrayList<>();
	public List<Float> weights = new ArrayList<>();
	
	//Used to indicate whether this node is valid in a given scenario
	public boolean valid = true;
	public boolean collide = false;
	
	public Node(int i, PVector p) {
		this.i = i;
		this.p = p;
	}
	
	
	public void clearEdges() {
		edges.clear();
		weights.clear();
	}
	
	
	/** Add an edge to the node with the given weight. */
	public void edge(Node node, float weight) {
		edges.add(node);
		weights.add(weight);
	}
	
	
	/** Add an edge to the node with the given weight,
	  * and do the same to the other node in question. */
	public void doubleEdge(Node node, float weight) {
		edge(node, weight);
		node.edge(this, weight);
	}
	
	
	/** For the purpose of the GUIs, equality is based on 2D position. */
	public boolean equals(Object o) {
		if (!(o instanceof Node)) return false;
		Node n = (Node) o;
		if (p.x == n.p.x && p.y == n.p.y)
			return true;
		return false;
	}
	
	
	/** Compare first on F value, then on H value. */
	@Override
	public int compareTo(Node n) {
		int c = Float.compare(f, n.f);
		if (c != 0) return c;
		return Float.compare(h, n.h);
	}
	
	
	/** Add movement and heuristic values to get F value. */
	public void f() {f = g + h;}

}