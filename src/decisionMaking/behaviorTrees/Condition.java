package decisionMaking.behaviorTrees;

import parameterization.AvailableActions;
import parameterization.Parameterization;
import decisionMaking.TreeNode;

public class Condition implements TreeNode {

	private String condition;
	private boolean trueState = true;
	
	public Condition(String condition) {
		this.condition = condition;
	}
	
	public Condition(String cold, boolean invert) {
		this(cold);
		trueState = !invert;
	}

	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		return p.get(condition) == trueState ? TRUE : FALSE;
	}

	@Override
	public String toString(int l) {
		return TreeNode.ident(l) + "Condition: " + (trueState ? "" : "!") + condition + '\n';
	}
	
}
