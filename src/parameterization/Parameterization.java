package parameterization;

import java.util.HashMap;

import entities.DecisionMaker;

public abstract class Parameterization {
	
	public static final String COIN_EXISTS = "CoinExists";
	public static final String BOMB_NEARBY = "BombNearby";
	public static final String QUICK_COIN = "QuickCoin";
	public static final String CROWDED = "Crowded";
	
	public static final String LINE_OF_SIGHT = "LineOfSight";
	public static final String LOW_HEALTH = "LowHealth";
	public static final String HIGH_HEALTH = "HighHealth";
	public static final String CAUGHT_PLAYER = "CaughtPlayer";
	public static final String COLD = "Cold";
	
	protected DecisionMaker entity;
	public void init(DecisionMaker entity) {this.entity = entity;}
	
	private HashMap<String, Boolean> parameters;
	public HashMap<String, Boolean> get() {return parameters;}
	public Boolean get(String parameter) {return parameters.get(parameter);}	
	
	public void parameterize() {
		this.parameters = new HashMap<String, Boolean>();
		parameterize(parameters);
	}
	
	protected abstract void parameterize(HashMap<String, Boolean> parameters);
}
