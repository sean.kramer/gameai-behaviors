package entities;

import processing.core.PApplet;
import util.Engine;

public class Explosion extends Entity {

	public static final float DEFAULT_BLAST = 200;
	
	private boolean active = false;
	private float boom;
	private float blast;
	
	public Explosion(float x, float y, float blast) {
		super(x, y);
		this.blast = blast;
	}
	
	public Explosion(float x, float y) {
		this(x, y, DEFAULT_BLAST);
	}

	@Override
	public void drawCrumb(PApplet g, float x, float y, float r) {
		// TODO Auto-generated method stub
	}

	@Override
	public void draw(PApplet g) {
		if (!active) return;
		boom += 5;
		boom = Math.min(boom, blast);
		
		int a = 255 - (int) (255 * boom / blast);
		a = Math.max(a, 20);
		a <<= 24;
		
		Engine.instance().damageArea(this, boom / 2, 1);
		
		g.noFill();
		g.strokeWeight(3);
		g.stroke(0x00ffffff | a);
		g.ellipse(position.x, position.y, boom, boom);
		
		if (boom >= blast)
			Engine.instance().removeEntity(this);
	}

	public void activate() {
		active = true;
	}
	
}
