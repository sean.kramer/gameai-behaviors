package parameterization;

import util.Engine;
import entities.Boid;
import entities.Coin;

public class MonsterActions extends AvailableActions {

	public boolean act(String action) {
		switch (action) {

		case SEEK_COIN:
			Coin nearest = (Coin) Engine.instance().nearest(Coin.class, entity);
			if (nearest == null) entity.wander();
			else entity.goal(Engine.world().quantize(nearest.position()));
			break;

		case WANDER:
			entity.wander();
			break;
			
		case IDLE:
			entity.idle();
			break;

		case SEEK_TARGET:
			entity.goal(Engine.world().quantize(Engine.instance().nearest(Boid.class, entity).position()));
			break;
		
		case COOLDOWN:
			entity.cooldown(120);
			
		case DANCE1:
			entity.rotate(10);
			break;
			
		case DANCE2:
			entity.rotate(-10);
			break;
			
		case ATTACK:
			Engine.instance().nearest(Boid.class, entity).damange(30);
			break;
			
		default:
			throw new IllegalArgumentException("No '" + action + "' found.");

		}
		entity.action(action);
		return true;
	}

}
