package decisionMaking;

import parameterization.AvailableActions;
import parameterization.Parameterization;

public interface TreeNode {

	public static final boolean DEBUG_DECISIONS = true;
	public static final boolean DEBUG_BEHAVIORS = true;
	
	public static final int FALSE = 0;
	public static final int TRUE = 1;
	public static final int ONGOING = 2;
	
	public static final String INDENT = "   ";
	public static String ident(int l) {
		String build = "";
		for (int i = 0; i < l; i++)
			build += INDENT;
		return build;
	}
	
	public int evaluate(Parameterization p, AvailableActions a, int l);
	public String toString(int l);
}
