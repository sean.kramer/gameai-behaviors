package behaviors;

import processing.core.PApplet;
import processing.core.PVector;
import entities.Entity;

public class SeekTarget extends BlendableBehavior {

	private PVector target;
	
	public SeekTarget(float x, float y, float strength) {
		super(strength);
		target(x, y);
	}
	
	
	public void target(PVector target) {
		this.target = target;
	}
	
	
	public void target(float x, float y) {
		target = new PVector(x, y);
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		if (target == null) return;
		PVector desired;
		if (PVector.dist(boid.position(), target) < 1.5f)
			desired = new PVector(-boid.velocity().x, -boid.velocity().y);
		else {
			desired = new PVector(target.x - boid.position().x, target.y - boid.position().y);
			desired.sub(boid.velocity());
			desired.normalize();
			desired.mult(strength);
		}
		boid.accelerate(desired);
	}

	
}
