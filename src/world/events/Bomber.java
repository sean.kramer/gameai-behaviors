package world.events;

import behaviors.Explode;
import processing.core.PVector;
import util.Engine;
import entities.Bomb;

public class Bomber extends WorldEvent {

	public static final int DEFAULT_RARITY = 30;
	public static final int DEFAULT_COOLDOWN = 30;
	
	public static final int MIN_EXPLODE = 180;
	public static final int EXPLODE_VAR = 120;
	
	public Bomber() {this(DEFAULT_RARITY, DEFAULT_COOLDOWN);}
	public Bomber(int rarity, int cooldown) {super(rarity, cooldown);}
	
	@Override
	protected void execute() {
		PVector loc = Engine.world().openPosition();
		Engine.instance().addEntity(new Bomb(loc.x, loc.y,
				new Explode(MIN_EXPLODE + r.nextInt(EXPLODE_VAR))
				));
	}
	
}
