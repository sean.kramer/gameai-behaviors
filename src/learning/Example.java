package learning;

public class Example {
	
	int total;
	int[] actions;	
	boolean[] states;
	
	public static int get(boolean... states) {
		int calc = 0;
		for (int i = 0; i < states.length; i++) {
			calc |= states[i] ? 1 : 0; 
			calc <<= 1;
		}
		return calc >> 1;
	}
	
	
	public Example(int states, int actions, int index) {
		this.states = new boolean[states];
		this.actions = new int[actions];
		for (int i = states - 1; i >= 0; i--) {
			this.states[i] = (index & 1) == 1;
			index >>= 1;
		}
	}
	
	
	public void add(int action) {
		if (action < 0) return;
		total++;
		actions[action]++;
	}
}
