package entities;

import processing.core.PApplet;

public class Crumb {
	float x, y, r;
	
	public Crumb(float x, float y, float r) {
		this.x = x;
		this.y = y;
		this.r = r;
	}
	
	public void draw(Entity boid, PApplet g) {
		boid.drawCrumb(g, x, y, r);
	}
}
