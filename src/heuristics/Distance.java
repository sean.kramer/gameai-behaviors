package heuristics;

import processing.core.PVector;
import graphs.Node;

public class Distance extends Heuristic {
	public float calc(Node current, Node end) {return PVector.dist(current.p, end.p);}
	public String getName() {return "Linear Distance";}
}
