 package world.events;

import java.util.Random;

public abstract class WorldEvent {

	private int rarity;
	private int cooldown;
	private int hot;
	protected static final Random r = new Random();
	
	
	public WorldEvent(int rarity, int cooldown) {
		this.rarity = rarity;
		this.cooldown = cooldown;
	}
	
	
	public void update() {
		if (hot > 0) hot--;
		else if (r.nextInt(rarity) == 0) {
			hot = cooldown;
			execute();
		}
	}
	
	
	protected abstract void execute();
	
}
