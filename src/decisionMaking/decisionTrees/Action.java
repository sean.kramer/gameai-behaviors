package decisionMaking.decisionTrees;

import decisionMaking.TreeNode;
import parameterization.AvailableActions;
import parameterization.Parameterization;

public class Action implements TreeNode {

	public String action;
	
	public Action(String action) {
		this.action = action;
	}
	
	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		Decision.build += action;
		a.act(action);
		return TRUE;
	}

	@Override
	public String toString(int l) {
		String build = "Action:" + action + '\n';
		return build;
	}

}
