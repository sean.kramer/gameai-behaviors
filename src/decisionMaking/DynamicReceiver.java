package decisionMaking;

import entities.DecisionMaker;

public class DynamicReceiver implements DecisionRetreiver {

	private TreeNode root;
	public DynamicReceiver(TreeNode root) {
		this.root = root;
	}
	
	
	@Override
	public TreeNode get(DecisionMaker entity) {
		return root;
	}

}
