package runnable;

import javax.swing.JOptionPane;

import parameterization.PlayerActions;
import parameterization.PlayerParameters;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import util.Simulation;
import world.World;
import world.events.Bomber;
import world.events.CoinDropper;
import behaviors.AlignToVelocity;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.CollectCoins;
import decisionMaking.Collector;
import entities.Boid;

public class DecisionTreeSim extends Simulation {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;

	private World world;
	
	private Boid boid;
	private BreadCrumbs crumb;
	

	public DecisionTreeSim() {
		super();
		crumb = new BreadCrumbs(20, 40);
		boid = new Boid(200, 200, 6, 1, 0xff000000, 0,
				
				new Collector(),
				new PlayerParameters(),
				new PlayerActions(),
				
				crumb,
				new CollectCoins(),
				new AlignToVelocity(),
				new CapAcceleration(0.2f),
				new CapRotation(PApplet.radians(6)),
				new CapVelocity(1)
				);
		
		engine.addEntity(boid);	
		engine.begin();
	}

	
	@Override
	public void mouseDragged(MouseEvent e) {
		world.adjust(e.getX(), e.getY(), shifting);
	}

	
	@Override
	public void draw(PGraphics g) {
	}

	
	public static String toFileName(String name) {
		if (name == null) return null;
		return "data/" + name.split("\\.")[0] + ".map";
	}
	


	@Override
	public void mousePressed(MouseEvent e) {
		world.adjust(e.getX(), e.getY(), shifting);
	}


	private boolean shifting = false;

	
	@Override
	public void keyReleased(KeyEvent e) {
		shifting = e.isShiftDown();
	}

	
	@Override
	public void keyPressed(KeyEvent e) {
		shifting = e.isShiftDown();
		switch (e.getKey()) {
		case 'r':
			crumb.clear();
			world.clear();
			break;
		case 'p':
			world.write(toFileName((String) JOptionPane.showInputDialog(null)));
			break;
		case 'o':
			world.openfile(toFileName((String) JOptionPane.showInputDialog(null)));
			break;
		default:
			//int i = e.getKey() - '1';
			//TODO terrain types
		}
	}


	@Override
	public String getName() {
		return "Decision Trees";
	}


	public static void main(String[] args) {new DecisionTreeSim();}


	@Override
	public World getWorld() {
		this.world = new World(
				new CoinDropper(),
				new Bomber()
				);;
		return world;
	}
}
