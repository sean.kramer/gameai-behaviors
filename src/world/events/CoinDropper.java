package world.events;

import processing.core.PVector;
import util.Engine;
import entities.Coin;

public class CoinDropper extends WorldEvent {

	public static final int DEFAULT_RARITY = 50;
	public static final int DEFAULT_COOLDOWN = 10;
	
	public CoinDropper() {this(DEFAULT_RARITY, DEFAULT_COOLDOWN);}
	public CoinDropper(int rarity, int cooldown) {super(rarity, cooldown);}
	
	@Override
	protected void execute() {
		PVector loc = Engine.world().openPosition();
		Engine.instance().addEntity(new Coin(loc.x, loc.y));
	}

}
