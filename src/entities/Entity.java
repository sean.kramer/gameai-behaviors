package entities;
import java.util.ArrayList;

import behaviors.BehaviorTemplate;
import processing.core.PApplet;
import processing.core.PVector;
import util.Engine;


public abstract class Entity {
	
	public static final int DEFAULT_SIZE = 6;
	
	protected PVector position;
	protected PVector velocity;
	protected float rotation;
	protected float mass;
	protected float size;
	protected int health = 100;
	protected int maxHealth = 100;
	protected float percent;	//For behaviors
	
	protected PVector deltaAcceleration;
	protected float deltaRotation;

	protected ArrayList<BehaviorTemplate> behaviors;
	
	public Entity(float x, float y, BehaviorTemplate... behaviors) {
		this(x, y, 1, DEFAULT_SIZE, 0, behaviors);
	}
	
	
	public void die() {}
	
	
	public float getMass() {
		return mass;
	}
	
	public Entity(float x, float y, float s, float m, float r, BehaviorTemplate... behaviors) {
		position = new PVector(x, y);
		velocity = new PVector(0, 0);
		rotation = r;
		mass = m;
		size = s;
		
		deltaAcceleration = new PVector(0, 0);
		deltaRotation = 0;
		
		this.behaviors = new ArrayList<BehaviorTemplate>(behaviors.length);
		for (BehaviorTemplate b : behaviors)
			this.behaviors.add(b);
	}
	
	
	
	public abstract void drawCrumb(PApplet g, float x, float y, float r);
	
	
	public void update() {
		rotation += deltaRotation;
		deltaRotation = 0;
		
		if (rotation > PApplet.PI)
			rotation -= PApplet.TWO_PI;
		else if (rotation <= -PApplet.PI)
			rotation += PApplet.TWO_PI;
		
		velocity.add(deltaAcceleration);
		deltaAcceleration.mult(0);
		
		position.add(velocity);
		Engine.world().collision(this);
	}
	
	
	public void applyBehaviors(PApplet g) {
		for (BehaviorTemplate b : behaviors)
			b.apply(this, g);
	}
	
	
	/** For getting current values. */
	public float percent() {return percent;}
	public float rotation() {return rotation;}
	public PVector velocity() {return velocity;}
	public PVector position() {return position;}
	public float size() {return size;}
	public int health() {return health;}
	public int maxHealth() {return maxHealth;}
	
	/** For progressively adjusting deltas */
	public void percent(float percent) {this.percent = percent;}
	public void rotate(float rotation) {deltaRotation += rotation;}
	public void accelerate(PVector acceleration) {deltaAcceleration.add(acceleration);}
	
	/** For getting delta values. */
	public float rotationDelta() {return deltaRotation;}
	public PVector acceleration() {return deltaAcceleration;}
	
	
	public void addBehavior(BehaviorTemplate behavior) {
		behaviors.add(behavior);
	}
	
	
	public abstract void draw(PApplet g);

	
	public void maxHealth(int max) {
		this.maxHealth = max;
		if (health > max) health = max;
	}
	
	
	public void damange(int damage) {
		health -= damage;
		if (health < 0) {
			health = 0;
			die();
		}
		if (health > maxHealth) {
			health = maxHealth;
		}
	}
	

	public void setPosition(PVector position) {
		this.position = position;
	}
}
