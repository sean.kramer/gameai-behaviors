package entities;

import behaviors.BehaviorTemplate;
import processing.core.PApplet;
import util.Engine;

public class Coin extends Entity {

	public static final int DEFAULT_SIZE = 20;	
	
	
	public Coin(float x, float y, BehaviorTemplate... behaviors) {
		this(x, y, DEFAULT_SIZE, behaviors);
	}
	
	
	public Coin(float x, float y, float s,
			BehaviorTemplate... behaviors) {
		super(x, y, s, 1, 0, behaviors);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void drawCrumb(PApplet g, float x, float y, float r) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void damange(int damage) {
		Engine.instance().removeEntity(this);
	}
	

	@Override
	public void draw(PApplet g) {
		g.noStroke();
		g.fill(0xffAF8400);
		g.ellipse(position.x, position.y, size, size);
		g.fill(0xffFED627);
		g.ellipse(position.x, position.y, size * .8f, size * .8f);
		g.fill(0xffAF8400);
		g.rect(position.x - size / 8, position.y - size / 4, size / 4, size / 2);
	}
	
	
}
