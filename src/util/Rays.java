package util;

import processing.core.PVector;


public class Rays {

	public static final float ERR = 0.001f;

	
	public static PVector intersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
		if (d == 0) return null;
		float a = x1 * y2 - y1 * x2;
		float b = x3 * y4 - y3 * x4;
		return new PVector((a * (x3 - x4) - b * (x1 - x2)) / d, (a * (y3 - y4) - b * (y1 - y2)) / d);
	}
	
	
	public static boolean onLine(float x1, float y1, float x2, float y2, PVector p) {
		return p.x >= Math.min(x1, x2) - ERR && p.x <= Math.max(x1, x2) + ERR
				&& p.y >= Math.min(y1, y2) - ERR && p.y <= Math.max(y1, y2) + ERR;
	}
	
	
	public static boolean segmentIntersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		PVector point = intersection(x1, y1, x2, y2, x3, y3, x4, y4);
		return point != null && onLine(x1, y1, x2, y2, point) &&  onLine(x3, y3, x4, y4, point);
	}
	
	
	public static PVector segmentIntersectionPoint(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
		PVector point = intersection(x1, y1, x2, y2, x3, y3, x4, y4);
		if (point != null && onLine(x1, y1, x2, y2, point) &&  onLine(x3, y3, x4, y4, point))
			return point;
		return null;
	}	
}
