package parameterization;

import entities.DecisionMaker;

public abstract class AvailableActions {
	
	public static final String IDLE = "Idle";
	public static final String SEEK_COIN = "SeekCoin";
	public static final String SEEK_BOMB = "SeekBomb";
	public static final String FLEE_BOMB = "FleeBomb";

	public static final String WANDER = "Wander";
	public static final String SEEK_TARGET = "SeekTarget";
	public static final String ATTACK = "Attack";
	public static final String DANCE1 = "Dance1";
	public static final String DANCE2 = "Dance2";
	public static final String DANCE3 = "Dance3";
	public static final String COOLDOWN = "Cooldown";
	
	
	protected DecisionMaker entity;
	public void init(DecisionMaker entity) {this.entity = entity;}
	
	public abstract boolean act(String action);
}
