package behaviors;

import processing.core.PApplet;
import util.Engine;
import entities.Coin;
import entities.Entity;

public class CollectCoins extends BehaviorTemplate {
	
	@Override
	public void apply(Entity boid, PApplet g) {
		Coin nearest = (Coin) Engine.instance().nearest(Coin.class, boid);
		if (nearest == null) return;
		if (boid.position().dist(nearest.position()) < boid.size()) {
			Engine.instance().removeEntity(nearest);
			Engine.instance().score(1);
		}
	}

	
}
