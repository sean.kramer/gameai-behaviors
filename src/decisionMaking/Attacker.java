package decisionMaking;

import parameterization.AvailableActions;
import parameterization.Parameterization;
import decisionMaking.behaviorTrees.Behavior;
import decisionMaking.behaviorTrees.Condition;
import decisionMaking.behaviorTrees.QuickBehavior;
import decisionMaking.behaviorTrees.RandomSelector;
import decisionMaking.behaviorTrees.Selector;
import decisionMaking.behaviorTrees.Sequencer;
import entities.DecisionMaker;

public class Attacker implements DecisionRetreiver {

	@Override
	public TreeNode get(DecisionMaker entity) {
		Selector root = new Selector();
		
		Sequencer health = new Sequencer();
		health.addChildren(
				new Condition(Parameterization.LOW_HEALTH),
				new Behavior(AvailableActions.SEEK_COIN, Parameterization.HIGH_HEALTH)
		);
		
		Selector target = new Selector();
		Sequencer sight = new Sequencer();
		
		Sequencer danceCheck = new Sequencer();
		RandomSelector dance = new RandomSelector();
		dance.addChildren(new Behavior(AvailableActions.DANCE1, Parameterization.COLD),
			new Behavior(AvailableActions.DANCE2, Parameterization.COLD));
		danceCheck.addChildren(new Condition(Parameterization.COLD, true), dance);
		
		sight.addChildren(
				new Condition(Parameterization.LINE_OF_SIGHT),
				new Behavior(AvailableActions.SEEK_TARGET, Parameterization.CAUGHT_PLAYER, Parameterization.LINE_OF_SIGHT, false, true),
				new Condition(Parameterization.CAUGHT_PLAYER),
				new QuickBehavior(AvailableActions.ATTACK),
				new QuickBehavior(AvailableActions.IDLE),
				new QuickBehavior(AvailableActions.COOLDOWN)
				);
		
		target.addChildren(sight, new QuickBehavior(AvailableActions.WANDER));
		root.addChildren(danceCheck, health, target);
		
		//System.out.println(root.toString(0));
		
		return root;
	}

}
