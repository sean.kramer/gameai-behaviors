package decisionMaking.behaviorTrees;

import parameterization.AvailableActions;
import parameterization.Parameterization;

public class Sequencer extends Picker {

	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		int result = children.get(state).evaluate(p, a, l + 1);
		while (result == TRUE) {
			state++;
			if (state == children.size()) {
				state = 0;
				return TRUE;
			}
			result = children.get(state).evaluate(p, a, l + 1);
		}
		if (result == FALSE)
			state = 0;
		return result;
	}

}
