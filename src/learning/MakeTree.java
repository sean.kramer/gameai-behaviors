package learning;

import java.util.ArrayList;
import java.util.HashMap;

import decisionMaking.TreeNode;
import decisionMaking.decisionTrees.Action;
import decisionMaking.decisionTrees.Decision;
import entities.DecisionMaker;

public class MakeTree {

	ArrayList<String> parameters = new ArrayList<>();
	ArrayList<String> actions = new ArrayList<>();
	
	DecisionMaker subject;
	Example[] examples;	
	
	public MakeTree(DecisionMaker subject, int parameters, String... states) {
		this.subject = subject;
		for (int i = 0; i < parameters; i++)
			this.parameters.add(states[i]);
		for (int i = parameters; i < states.length; i++)
			this.actions.add(states[i]);
		
		examples = new Example[(int) Math.pow(2, parameters)];
		for (int i = 0; i < examples.length; i++) 
			examples[i] = new Example(parameters, actions.size(), i);
	}
	
	
	public void inform(String action, HashMap<String, Boolean> info) {
		if (info == null) return;
		int act = actions.indexOf(action);
		boolean[] states = new boolean[parameters.size()];
		for (int i = 0; i < parameters.size(); i++)
			states[i] = info.get(parameters.get(i));
		examples[Example.get(states)].add(act);
	}
	
	
	@SuppressWarnings("unchecked")
	public TreeNode make(ArrayList<Example> examples, ArrayList<Integer> attributes) {
		double initialEntropy = entropy(examples);

		int hit = 0;
		int[] actions = countActions(examples);
		int best = 0;
		if (actions[0] > 0) hit++;
		for (int i = 1; i < actions.length; i++) {
			if (actions[i] > 0) hit++;
			if (actions[i] > actions[best])
				best = i;
		}
		
		if (initialEntropy <= 0 || hit == 1)
			return new Action(this.actions.get(best));

		int exampleCount = exampleLength(examples);

		double bestInformationGain = 0;
		int bestSplitAttribute = 0;
		ArrayList<Example>[] bestSets = null;

		for (int attribute : attributes) {
			ArrayList<Example>[] sets = splitByAttribute(examples, attribute);
			double overallEntropy = entropyOfSets(sets, exampleCount);
			double informationGain = initialEntropy + overallEntropy;
			
			if (informationGain > bestInformationGain) {
				bestInformationGain = informationGain;
				bestSplitAttribute = attribute;
				bestSets = sets;
			}
		}

		Decision decision = new Decision(parameters.get(bestSplitAttribute));
		attributes.remove(new Integer(bestSplitAttribute));
		decision.setChildren(
				make(bestSets[0], (ArrayList<Integer>) attributes.clone()),
				make(bestSets[1], (ArrayList<Integer>) attributes.clone())
				);
		return decision;
	}
	
	
	private int exampleLength(ArrayList<Example> examples) {
		int count = 0;
		for (Example e : examples)
			count += e.total;
		return count;
	}
	
	
	private double entropyOfSets(ArrayList<Example>[] sets, int exampleCount) {
		double entropy = 0;
		for (ArrayList<Example> set : sets) 
			entropy -= (exampleLength(set) / (double) exampleCount) * entropy(set);
		return entropy;
	}


	public void update() {
		inform(subject.action(), subject.getParameters());
	}
	
	
	public TreeNode generate() {
		ArrayList<Example> examples = new ArrayList<Example>();
		for (Example e : this.examples)
			if (e.total > 0)
				examples.add(e);
		
		ArrayList<Integer> attributes = new ArrayList<>();
		for (int i = 0; i < this.parameters.size(); i++)
			attributes.add(i);
		
		return make(examples, attributes);
	}
	
	
	private int[] countActions(ArrayList<Example> examples) {
		int[] actions = new int[this.actions.size()];
		for (Example e : examples) {
			for (int i = 0; i < actions.length; i++)
				actions[i] += e.actions[i];
		}
		return actions;
	}
	
	
	private double entropy(ArrayList<Example> examples) {
		//No entropy
		if (examples.size() <= 1) return 0;
		
		//Tally 
		int total = exampleLength(examples);
		int[] actions = countActions(examples);
		
		//Check for multiple actions
		int hit = 0;
		for (int i : actions) {
			if (i > 0) {
				hit++;
				if (hit > 1) break;
			}
		}
		if (hit <= 1) return 0;
		
		//Calculate
		double entropy = 0;
		for (int i : actions) {
			if (i == 0) continue;
			double proportion = i / (double) total;
			entropy -= proportion * Math.log(i / (double) total) / Math.log(2);
		}
		
		return entropy;
	}
	
	
	@SuppressWarnings("unchecked")
	private ArrayList<Example>[] splitByAttribute(ArrayList<Example> examples, int attribute) {
		ArrayList<Example>[] sets = new ArrayList[2];
		sets[0] = new ArrayList<>();
		sets[1] = new ArrayList<>();
		for (Example e : examples) {
			if (e.states[attribute]) sets[0].add(e);
			else sets[1].add(e);
		}
		
		return sets;
	}
	
}
