package parameterization;

import java.util.HashMap;

import util.Engine;
import entities.Boid;
import entities.Coin;
import entities.Explosion;

public class PlayerParameters extends Parameterization {

	public void parameterize(HashMap<String, Boolean> parameters) {		
		float dist = Engine.instance().nearestDist(Explosion.class, entity); 
		parameters.put(BOMB_NEARBY, dist >= 0 && dist <= Explosion.DEFAULT_BLAST * 0.6f);

		dist = Engine.instance().nearestDist(Coin.class, entity);
		parameters.put(COIN_EXISTS, dist >= 0);
		parameters.put(QUICK_COIN, dist >= 0 && dist <= 40);
		
		dist = Engine.instance().nearestDist(Boid.class, entity);
		parameters.put(CROWDED, dist >= 0 && dist <= 80);
	}
	
}
