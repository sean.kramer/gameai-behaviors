package parameterization;

import java.util.HashMap;

import util.Engine;
import entities.Boid;


public class MonsterParameters extends Parameterization {

	@Override
	public void parameterize(HashMap<String, Boolean> parameters) {
		
		Boid nearest = (Boid) Engine.instance().nearest(Boid.class, entity);
		
		parameters.put(LINE_OF_SIGHT, nearest != null && Engine.world().rayCast(entity.position(), nearest.position()));
		
		float dist = Engine.instance().nearestDist(Boid.class, entity);
		parameters.put(CAUGHT_PLAYER, dist >= 0 && dist < entity.size() * 1.5f);
		
		parameters.put(LOW_HEALTH, entity.health() < entity.maxHealth() * 0.6f);
		parameters.put(HIGH_HEALTH, entity.health() >= entity.maxHealth() * 0.8f);
		
		parameters.put(COLD, entity.cooldown() == 0);
	}


	
}
