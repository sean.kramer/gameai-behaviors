package decisionMaking.behaviorTrees;

import java.util.ArrayList;

import decisionMaking.TreeNode;

public abstract class Picker implements TreeNode {

	protected int state = 0;
	ArrayList<TreeNode> children = new ArrayList<>();
	
	public void addChildren(TreeNode... tasks) {
		for (TreeNode task : tasks)
			children.add(task);
	}
	
	public String toString(int l) {
		String build = this.getClass().toString() + '\n';
		build = TreeNode.ident(l) + build.substring(build.lastIndexOf('.') + 1);
		for (TreeNode child : children)
			build += child.toString(l + 1);
		return build;
	}
}
