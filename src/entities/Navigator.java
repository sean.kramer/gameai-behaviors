package entities;

import graphs.Algorithm;
import processing.core.PApplet;
import processing.core.PVector;
import util.Engine;
import util.Grid;
import behaviors.BehaviorTemplate;
import behaviors.SeekTarget;

public abstract class Navigator extends Entity {

	public Navigator(float x, float y, BehaviorTemplate[] behaviors) {
		super(x, y, behaviors);
	}

	public Navigator(float x, float y, float s, float m, float r,
			BehaviorTemplate[] behaviors) {
		super(x, y, s, m, r, behaviors);
	}


	private SeekTarget seekTarget = new SeekTarget(0, 0, 1);	
	private PVector target;
	private Algorithm alg;
	private Grid goal;

	public PVector target() {return target;}
	public Algorithm alg() {return alg;}
	public Grid goal() {return goal;}

	public void target(PVector target) {this.target = target;}
	public void alg(Algorithm alg) {this.alg = alg;}
	public void goal(Grid goal) {this.goal = goal;}

	
	public void idle() {
		alg = null;
		goal = null;
		target = this.position.get();
	}
	
	
	private Grid wander = null;
	public void wander() {
		//Keep wandering the same path
		if (alg != null && alg.getSolved() && wander != null && goal == wander) {
			Grid g = Engine.world().quantize(position());
			if (!wander.equals(g))
				return;
		}
		goal = Engine.world().quantize(Engine.world().openPosition());
		wander = goal;
	}
	
	
	int j = 0;
	@Override
	public void applyBehaviors(PApplet g) {
		Engine.world().navigate(this);
		seekTarget.target(target);
		seekTarget.apply(this, g);
		super.applyBehaviors(g);
	}

}
