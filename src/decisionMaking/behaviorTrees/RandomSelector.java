package decisionMaking.behaviorTrees;

import java.util.Random;

import parameterization.AvailableActions;
import parameterization.Parameterization;

public class RandomSelector extends Picker {

	private boolean[] stateList = null;
	private static Random r = new Random();
	
	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		if (stateList == null) {
			stateList = new boolean[children.size()];
			nextState();
		}
		
		int result = children.get(state).evaluate(p, a, l + 1);
		
		while (result == FALSE) {
			stateList[state] = true;
			nextState();
			if (state < 0) {
				stateList = null;
				return FALSE;
			}
			result = children.get(state).evaluate(p, a, l + 1);
		}
		if (result == TRUE)
			stateList = null;
		return result;
	}

	
	private void nextState() {
		int available = 0;
		for (boolean b : stateList)
			if (!b) available++;
		if (available == 0) {
			state = -1;
			return;
		}
		int s = r.nextInt(available);
		int i = 0;
		while (true) {
			if (!stateList[i]) {
				if (s == 0) break;
				s--;
			}
			i++;
		}
		state = i;
	}
}
