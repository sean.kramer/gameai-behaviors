package graphics;

import processing.core.PGraphics;
import entities.Entity;

public class HealthBar {

	public static void draw(PGraphics g, Entity entity) {
		float percent = (float) entity.health() / entity.maxHealth();
		g.noStroke();
		g.fill(0xff444444);
		g.rect(-entity.size() * 2 - 1, entity.size() * 1.8f - 1, entity.size() * 4 + 2, entity.size() / 2 + 2);
		g.fill(0xffff0000);
		g.rect(-entity.size() * 2, entity.size() * 1.8f, entity.size() * 4, entity.size() / 2);
		g.fill(0xff00ff00);
		g.rect(-entity.size() * 2, entity.size() * 1.8f, entity.size() * 4 * percent, entity.size() / 2);
	}
	
}
