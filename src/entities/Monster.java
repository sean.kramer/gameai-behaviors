package entities;
import graphics.HealthBar;
import parameterization.AvailableActions;
import parameterization.Parameterization;
import processing.core.PApplet;
import util.Engine;
import behaviors.BehaviorTemplate;
import decisionMaking.DecisionRetreiver;


public class Monster extends DecisionMaker {
	public static final float CRUMB_SCALE = 0.4f;
	
	//Color
	private int c;
	
	private float radius = 10;
	private float diameter = radius * 2;
	private float tx = radius * PApplet.cos(PApplet.radians(60));
	private float ty = radius * PApplet.sin(PApplet.radians(60));
	
	private float bdiameter = diameter * CRUMB_SCALE;
	private float btx = tx * CRUMB_SCALE;
	private float bty = ty * CRUMB_SCALE;
	
	private void assign() {
		diameter = radius * 2;
		tx = radius * PApplet.cos(PApplet.radians(60));
		ty = radius * PApplet.sin(PApplet.radians(60));

		bdiameter = diameter * CRUMB_SCALE;
		btx = tx * CRUMB_SCALE;
		bty = ty * CRUMB_SCALE;
	}

	
	public Monster(float x, float y, float s, float m, int c, int r,
			DecisionRetreiver tree,
			Parameterization parameterization,
			AvailableActions availableActions,
			BehaviorTemplate... behavior) {
		super(x, y, s, m, r,
				tree,
				parameterization,
				availableActions,
				behavior);
		this.c = c;
		radius = s;
		assign();
	}
	
	
	@Override
	public void draw(PApplet g) {
		g.noStroke();
		g.pushMatrix();
		g.translate(position.x, position.y);
		
		HealthBar.draw(g.g, this);

		g.fill(c);
		g.ellipse(0, 0, diameter, diameter);
		g.rotate(rotation);
		g.triangle(tx, -ty, diameter, 0, tx, ty);
		g.popMatrix();
		
	}
	
	
	@Override
	public void die() {
		Engine.instance().removeEntity(this);
	}
	

	@Override
	public void drawCrumb(PApplet g, float x, float y, float r) {
		g.noStroke();
		g.fill(c);
		g.pushMatrix();
		g.translate(x, y);
		g.ellipse(0, 0, bdiameter, bdiameter);
		g.rotate(r);
		g.triangle(btx, -bty, bdiameter, 0, btx, bty);
		g.popMatrix();
	}

	
	public void color(int color) {
		this.c = color;
	}
	
	
	public void setPosition(float x, float y) {
		position.x = x;
		position.y = y;
		velocity.mult(0);
	}
}
