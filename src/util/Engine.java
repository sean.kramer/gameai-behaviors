package util;
import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PVector;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import world.World;
import entities.Entity;


public class Engine extends PApplet {
	
	public static final long serialVersionUID = 1L;
	
	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;
	
	public static boolean drawBehaviors = false;
	
	private static Engine engine = null;
	private static World world;
	
	private static Simulation sim;
	private boolean running = false;

	private ArrayList<Entity> entities;
	private ArrayList<Entity> removeList;
	private ArrayList<Entity> addList;

	private int score = 0;

	public static Engine boot(Simulation simulation) {
		sim = simulation;
		PApplet.main("util.Engine");
		while (engine == null)
			try {Thread.sleep(10);} catch (Exception e) {}
		world = sim.getWorld();
		return engine;
	}
	
	public void begin() {
		running = true;
	}
	
	
	public static Engine instance() {
		return engine;
	}
	
	public void keyPressed(KeyEvent e) {sim.keyPressed(e);}
	public void keyReleased(KeyEvent e) {sim.keyReleased(e);}
	
	public void mouseMoved(MouseEvent e) {sim.mouseMoved(e);}
	public void mouseDragged(MouseEvent e) {sim.mouseDragged(e);}
	public void mousePressed(MouseEvent e) {sim.mousePressed(e);}
	public void mouseReleased(MouseEvent e) {sim.mouseReleased(e);}
	
	
	public void setup() {
		engine = this;
		entities = new ArrayList<Entity>();
		removeList = new ArrayList<Entity>();
		addList = new ArrayList<Entity>();
		
		size(WIDTH, HEIGHT);
		
		frame.setTitle(sim.getName());
		frame.setResizable(false);
		frameRate(60);
		
		ellipseMode(CENTER);
		smooth();
	}
	
	
	public void draw() {
		background(0xffcccccc);
		if (!running) return;
		
		for (Entity i : removeList)
			entities.remove(i);
		removeList.clear();
		for (Entity i : addList)
			entities.add(i);
		addList.clear();
		
		
		sim.draw(g);
		
		frame.setTitle(sim.getName() + " (Score: " + score + ")");
		
		for (Entity e : entities) {
			e.applyBehaviors(this);
			e.update();
		}
		
		for (Entity e : entities)
			e.draw(this);
		
		world.update();
		world.draw(g);
	}

	
	public void addEntity(Entity e) {addList.add(e);}
	public void removeEntity(Entity e) {removeList.add(e);}
	
	
	public List<Entity> entities() {return entities;}
	public static World world() {return world;}
	
	
	public static void main(String[] args) {
		System.err.println("Run as Application");
	}
	
	
	public void damageArea(Entity caster, float aoe, int damage) {
		for (Entity entity : entities) {
			if (entity == caster) continue;
			if (caster.position().dist(entity.position()) < aoe)
				entity.damange(damage);
		}
	}
	

	@SuppressWarnings("rawtypes")
	public Entity nearest(Class check, Entity asker) {
		return nearest(check, asker.position());
	}
	
	
	@SuppressWarnings("rawtypes")
	public Entity nearest(Class check, PVector position) {
		float dist = Float.MAX_VALUE;
		Entity nearest = null;
		for (Entity entity : entities) {
			if (!(check.isInstance(entity))) continue;
			float d = position.dist(entity.position());
			if (d >= dist) continue;
			dist = d;
			nearest = entity;
		}
		return nearest;
	}
	
	
	@SuppressWarnings("rawtypes")
	public float nearestDist(Class check, Entity asker) {
		return nearestDist(check, asker.position(), asker);
	}
	
	@SuppressWarnings("rawtypes")
	public float nearestDist(Class check, PVector position) {
		return nearestDist(check, position, null);
	}
	
	
	@SuppressWarnings("rawtypes")
	public float nearestDist(Class check, PVector position, Entity asker) {
		float dist = Float.MAX_VALUE;
		for (Entity entity : entities) {
			if (entity == asker) continue;
			if (!(check.isInstance(entity))) continue;
			float d = position.dist(entity.position());
			if (d >= dist) continue;
			dist = d;
		}
		if (dist == Float.MAX_VALUE)
			return -1;
		return dist;
	}
	
	
	@SuppressWarnings("rawtypes")
	public Grid safeFrom(Class check, Entity asker, float buffer) {
		return safeFrom(check, asker.position(), buffer);
	}
	
	@SuppressWarnings("rawtypes")
	public Grid safeFrom(Class check, PVector position, float buffer) {
		float distFromBomb = Float.MAX_VALUE;
		float distFromBoid = Float.MAX_VALUE;
		
		Grid best = null;
		for (int i = 1; i < 100; i++) {
			int delta;
			for (int y = -i; y <= i; y++) {
				float yy = y * World.BOUND + position.y;
				delta = (y == i || y == -i) ? 1 : i + i;
				for (int x = -i; x <= i; x += delta) {
					float xx = x * World.BOUND + position.x;
					Grid grid = world.quantize(xx, yy); 
					PVector loc = new PVector(xx, yy);
					if (grid == null || !world.nodify(grid).valid) continue;
					float dist = nearestDist(check, loc);
					
					//Take whatever we can get away from bomb
					if (distFromBomb <= buffer) {
						if (dist < distFromBomb) continue;
						distFromBomb = dist;
						float tmp = position.dist(loc);
						if (tmp >= distFromBoid) continue;
						distFromBoid = position.dist(loc);
						best = grid;
						
					//We've found a good spot already. Try to find a better one.
					} else if (dist > buffer) {
						float tmp = position.dist(loc);
						if (tmp >= distFromBoid) continue;
						distFromBoid = tmp;
						best = grid;
					}
				}
			}
			if (distFromBomb < Float.MAX_VALUE && distFromBomb > buffer)
				return best;
		}
		return best;
	}
	
	
	public boolean entityExists(Class<?> checker) {
		for (Entity entity : entities)
			if (checker.isInstance(entity))
				return true;
		return false;
	}

	public void score(int delta) {
		score  += delta;
	}

	public int count(Class<?> checker) {
		int count = 0;
		for (Entity entity : entities)
			if (checker.isInstance(entity))
				count++;
		return count;
	}
}

