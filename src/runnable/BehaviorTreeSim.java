package runnable;

import javax.swing.JOptionPane;

import parameterization.MonsterActions;
import parameterization.MonsterParameters;
import parameterization.PlayerActions;
import parameterization.PlayerParameters;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import util.Engine;
import util.Simulation;
import world.World;
import world.events.BoidMaker;
import world.events.Bomber;
import world.events.CoinDropper;
import behaviors.AlignToVelocity;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.CollectCoins;
import decisionMaking.Attacker;
import decisionMaking.Collector;
import entities.Boid;
import entities.Coin;
import entities.Entity;
import entities.Monster;

public class BehaviorTreeSim extends Simulation {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;

	private World world;

	private Boid boid;
	
	public BehaviorTreeSim() {
		super();
		boid = new Boid(200, 200, 6, 1, 0xff000000, 0,
				
				new Collector(),
				new PlayerParameters(),
				new PlayerActions(),
				
				new BreadCrumbs(20, 20),
				new CollectCoins(),
				new AlignToVelocity(),
				new CapAcceleration(0.2f),
				new CapRotation(PApplet.radians(6)),
				new CapVelocity(1.2f)
				);
		
		
		Monster monster = new Monster(400, 400, 8, 1, 0xffff0000, 0,
				
				new Attacker(),
				new MonsterParameters(),
				new MonsterActions(),

				new BreadCrumbs(20, 20),
				new CollectCoins() {
					@Override
					public void apply(Entity boid, PApplet g) {
						Coin nearest = (Coin) Engine.instance().nearest(Coin.class, boid);
						if (nearest == null) return;
						if (boid.position().dist(nearest.position()) < boid.size()) {
							Engine.instance().removeEntity(nearest);
							if (boid.health() == boid.maxHealth())
								Engine.instance().score(-1);
							else boid.damange(-20);
						}
					}
				},
				new AlignToVelocity(),
				new CapAcceleration(0.2f),
				new CapRotation(PApplet.radians(6)),
				new CapVelocity(1.3f)
				);
		
		monster.damange(30);
		
		engine.addEntity(boid);
		engine.addEntity(monster);
		engine.begin();
	}

	
	@Override
	public void mouseDragged(MouseEvent e) {
		world.adjust(e.getX(), e.getY(), shifting);
	}

	
	@Override
	public void draw(PGraphics g) {
	}

	
	public static String toFileName(String name) {
		if (name == null) return null;
		return "data/" + name.split("\\.")[0] + ".map";
	}
	


	@Override
	public void mousePressed(MouseEvent e) {
		world.adjust(e.getX(), e.getY(), shifting);
	}


	private boolean shifting = false;

	
	@Override
	public void keyReleased(KeyEvent e) {
		shifting = e.isShiftDown();
	}

	
	@Override
	public void keyPressed(KeyEvent e) {
		shifting = e.isShiftDown();
		switch (e.getKey()) {
		case 'r':
			world.clear();
			break;
		case 'p':
			world.write(toFileName((String) JOptionPane.showInputDialog(null)));
			break;
		case 'o':
			world.openfile(toFileName((String) JOptionPane.showInputDialog(null)));
			break;
		default:
			//int i = e.getKey() - '1';
			//TODO terrain types
		}
	}


	@Override
	public String getName() {
		return "Behavior Trees";
	}


	public static void main(String[] args) {new BehaviorTreeSim();}


	@Override
	public World getWorld() {
		this.world = new World(
				new CoinDropper(),
				new Bomber(30, 10),
				new BoidMaker()
				);
		return world;
	}
}
