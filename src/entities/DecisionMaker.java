package entities;

import java.util.HashMap;

import parameterization.AvailableActions;
import parameterization.Parameterization;
import processing.core.PApplet;
import behaviors.BehaviorTemplate;
import decisionMaking.TreeNode;
import decisionMaking.DecisionRetreiver;

public abstract class DecisionMaker extends Navigator {

	TreeNode tree;
	Parameterization parameterization;
	AvailableActions availableActions;
	
	private String action = null;
	
	private int cooldown = 0;
	
	public DecisionMaker(float x, float y, float s, float m, float r,
			DecisionRetreiver tree,
			Parameterization parameterization,
			AvailableActions availableActions,
			BehaviorTemplate[] behaviors) {
		super(x, y, s, m, r, behaviors);
		this.tree = tree.get(this);
		this.parameterization = parameterization;
		this.availableActions = availableActions;
		parameterization.init(this);
		availableActions.init(this);
	}	
	

	@Override
	public void applyBehaviors(PApplet g) {
		if (cooldown > 0)
			cooldown--;
		parameterization.parameterize();
		if (tree.evaluate(parameterization, availableActions, 0) == TreeNode.FALSE)
			idle();
		super.applyBehaviors(g);
	}
	
	
	public void cooldown(int cooldown) {
		this.cooldown += cooldown;
	}


	public int cooldown() {return cooldown;}
	
	public HashMap<String, Boolean> getParameters() {
		return parameterization.get();
	}


	public String action() {
		return action;
	}


	public void action(String action) {
		this.action = action;
	}
}
