package decisionMaking;

import decisionMaking.decisionTrees.Action;
import decisionMaking.decisionTrees.Decision;
import parameterization.AvailableActions;
import parameterization.Parameterization;
import entities.DecisionMaker;

public class Collector implements DecisionRetreiver {

	public TreeNode get(DecisionMaker entity) {
		Decision collector = new Decision(Parameterization.CROWDED);
		Decision normal = new Decision(Parameterization.COIN_EXISTS);
		Action collect = new Action(AvailableActions.SEEK_COIN);		
		Action idle = new Action(AvailableActions.WANDER);		
		normal.setChildren(collect, idle);
		collector.setChildren(new Action(AvailableActions.WANDER), normal);
		
		
		Decision root = new Decision(Parameterization.BOMB_NEARBY);
		Decision dip = new Decision(Parameterization.QUICK_COIN);
		root.setChildren(dip, collector);
		
		Action fleeBomb = new Action(AvailableActions.FLEE_BOMB);
		Action quickCoin = new Action(AvailableActions.SEEK_COIN);
		dip.setChildren(quickCoin, fleeBomb);

		//System.out.println(root.toString(0));
		
		return root;
	}
	
}
