package decisionMaking.behaviorTrees;

import parameterization.AvailableActions;
import parameterization.Parameterization;
import decisionMaking.TreeNode;

public class QuickBehavior implements TreeNode {

	String action;
	
	public QuickBehavior(String action) {
		this.action = action;
	}
	
	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		if (DEBUG_BEHAVIORS) System.out.println(action);
		return a.act(action) ? TRUE : FALSE;
	}

	@Override
	public String toString(int l) {
		return TreeNode.ident(l) + "QuickBehavior: " + action + '\n';
	}
	
	
}
