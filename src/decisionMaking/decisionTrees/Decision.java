package decisionMaking.decisionTrees;

import decisionMaking.TreeNode;
import parameterization.AvailableActions;
import parameterization.Parameterization;


public class Decision implements TreeNode {

	public static String last = null;
	public static String build;

	
	String condition;
	TreeNode trueChild, falseChild;
	
	
	public Decision(String condition) {
		this.condition = condition;
	}
	
	
	public void setChildren(TreeNode trueChild, TreeNode falseChild) {
		this.trueChild = trueChild;
		this.falseChild = falseChild;
	}
	

	@SuppressWarnings("unused")
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		if (l == 0) build = "";
		boolean result = p.get(condition);
		build += condition + ":" + result + ">";
		if (result) trueChild.evaluate(p, a, l + 1);
		else falseChild.evaluate(p, a, l + 1);
		if (!DEBUG_DECISIONS || l > 0 || build.equals(last)) return TRUE;
		System.out.println(build);
		last = build;
		return TRUE;
	}


	@Override
	public String toString(int l) {
		String build = "";
		build += "Decision:" + condition + '\n';
		build += TreeNode.ident(l + 1);
		build += trueChild.toString(l + 1);
		build += TreeNode.ident(l + 1);
		build += falseChild.toString(l + 1);
		return build;
	}
	
}
