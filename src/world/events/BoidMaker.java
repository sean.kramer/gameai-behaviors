package world.events;

import parameterization.MonsterActions;
import parameterization.MonsterParameters;
import parameterization.PlayerActions;
import parameterization.PlayerParameters;
import processing.core.PApplet;
import processing.core.PVector;
import util.Engine;
import behaviors.AlignToVelocity;
import behaviors.BreadCrumbs;
import behaviors.CapAcceleration;
import behaviors.CapRotation;
import behaviors.CapVelocity;
import behaviors.CollectCoins;
import decisionMaking.Attacker;
import decisionMaking.Collector;
import entities.Boid;
import entities.Coin;
import entities.Entity;
import entities.Monster;

public class BoidMaker extends WorldEvent {

	public static final int DEFAULT_RARITY = 200;
	public static final int DEFAULT_COOLDOWN = 200;

	public BoidMaker() {this(DEFAULT_RARITY, DEFAULT_COOLDOWN);}
	public BoidMaker(int rarity, int cooldown) {super(rarity, cooldown);}

	@Override
	protected void execute() {
		if (Engine.instance().count(Monster.class) == 0) {
			Engine.instance().addEntity(new Monster(400, 400, 8, 1, 0xffff0000, 0,
				new Attacker(),
				new MonsterParameters(),
				new MonsterActions(),

				new BreadCrumbs(20, 20),
				new CollectCoins() {
				@Override
				public void apply(Entity boid, PApplet g) {
					Coin nearest = (Coin) Engine.instance().nearest(Coin.class, boid);
					if (nearest == null) return;
					if (boid.position().dist(nearest.position()) < boid.size()) {
						Engine.instance().removeEntity(nearest);
						boid.damange(-20);
					}
				}},
				new AlignToVelocity(),
				new CapAcceleration(0.2f),
				new CapRotation(PApplet.radians(6)),
				new CapVelocity(1.2f)
			));
		}

		if (Engine.instance().count(Boid.class) >= 4) return;

		PVector loc = Engine.world().openPosition();
		Boid boid = new Boid(loc.x, loc.y, 6, 1,
				Engine.world().random.nextInt() | 0xff000000 & 0xff0f0f0f, 0,

				new Collector(),
				new PlayerParameters(),
				new PlayerActions(),

				new BreadCrumbs(20, 20),
				new CollectCoins(),
				new AlignToVelocity(),
				new CapAcceleration(0.2f),
				new CapRotation(PApplet.radians(6)),
				new CapVelocity(1.2f)
				);

		Engine.instance().addEntity(boid);
	}

}
