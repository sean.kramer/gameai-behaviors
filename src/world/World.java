package world;

import entities.Boid;
import entities.Entity;
import entities.Navigator;
import graphs.Algorithm;
import graphs.Node;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

import processing.core.PGraphics;
import processing.core.PVector;
import util.Engine;
import util.Grid;
import util.Rays;
import world.events.WorldEvent;

public class World {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;

	public static final int BOUND = 20;
	private static final float HALF = BOUND / 2 + 0.001f;

	private Node[][] nodes;

	private ArrayList<WorldEvent> worldEvents;
	
	private int xOff, yOff;
	
	private boolean valid = false;	

	public Node[][] nodes() {
		return nodes;
	}

	
	public World(WorldEvent... events) {
		worldEvents = new ArrayList<WorldEvent>();
		for (WorldEvent event : events)
			worldEvents.add(event);
		initGrid();
	}


	private void initGrid() {
		nodes = new Node[(Engine.instance().height - BOUND * 2) / BOUND][(Engine.instance().width - BOUND * 2) / BOUND];
		xOff = BOUND + BOUND / 2 + ((Engine.instance().width - BOUND * 2) - (nodes[0].length * BOUND)) / 2;
		yOff = BOUND + BOUND / 2 + ((Engine.instance().height - BOUND * 2) - (nodes.length * BOUND)) / 2;
		int c = 0;
		for (int y = 0; y < nodes.length; y++) {
			for (int x = 0; x < nodes[0].length; x++) {
				nodes[y][x] = new Node(c, new PVector(xOff + x * BOUND, yOff + y * BOUND));
				c++;
			}
		}
		xOff -= BOUND / 2;
		yOff -= BOUND / 2;

		setEdges();
	}


	public void setEdges() {
		for (int y = 0; y < nodes.length; y++)
			for (int x = 0; x < nodes[0].length; x++)
				nodes[y][x].clearEdges();


		for (int y = 0; y < nodes.length; y++) {
			for (int x = 0; x < nodes[0].length; x++) {
				Node n = nodes[y][x];
				if (!n.valid) continue;
				if (x > 0 && nodes[y][x - 1].valid)
					n.doubleEdge(nodes[y][x - 1], PVector.dist(n.p, nodes[y][x - 1].p));
				if (y > 0 && nodes[y - 1][x].valid)
					n.doubleEdge(nodes[y - 1][x], PVector.dist(n.p, nodes[y - 1][x].p));

				if (x > 0 && y > 0 && nodes[y - 1][x - 1].valid && nodes[y][x - 1].valid && nodes[y - 1][x].valid)
					n.doubleEdge(nodes[y - 1][x - 1], PVector.dist(n.p, nodes[y - 1][x - 1].p));
				if (x > 0 && y < nodes.length - 1 && nodes[y + 1][x - 1].valid
						&& nodes[y + 1][x].valid && nodes[y][x - 1].valid)
					n.doubleEdge(nodes[y + 1][x - 1], PVector.dist(n.p, nodes[y + 1][x - 1].p));
			}
		}
	}


	public Algorithm solve(Navigator boid) {
		Node goal = nodify(boid.goal());
		Node cur = nodify(boid.position());
		Algorithm alg = new Algorithm(cur, goal);
		alg.step();
		boid.alg(alg);
		return alg;
	}

	
	public Grid quantize(PVector position) {
		return quantize(position.x, position.y);
	}
	

	public Grid quantize(float x, float y) {
		if (x < BOUND || x >= Engine.instance().width - BOUND || y < BOUND || y >= Engine.instance().height - BOUND)
			return null;
		int a = ((int) x - xOff) / BOUND;
		int b = ((int) y - yOff) / BOUND;
		if (a < 0) return null;
		if (b < 0) return null;
		if (a >= nodes[0].length) return null;
		if (b >= nodes.length) return null;
		return new Grid(a, b);
	}
	
	
	public void draw(PGraphics g) {
		g.noStroke();
		
		g.fill(0xff000066);
		g.rect(0, 0, BOUND, Engine.instance().height);
		g.rect(0, 0, Engine.instance().width, BOUND);
		g.rect(Engine.instance().width, 0, -BOUND, Engine.instance().height);
		g.rect(0, Engine.instance().height, Engine.instance().width, -BOUND);

		for (Node[] y : nodes) {
			for (Node x : y) {
				if (!x.valid) {
					g.fill(0xffa200cc);
					int i;
					for (i = 0; i < 5; i++)
						g.rect(x.p.x - HALF - i, x.p.y - HALF - i, BOUND + 1, BOUND);
					if (x.collide) {
						g.fill(0xffff00ff);
						x.collide = false;
					} else {
						g.fill(0xffb200ff);
					}
					i--;
					g.rect(x.p.x - HALF - i, x.p.y - HALF - i, BOUND, BOUND);
				}
			}
		}
	}


	public static String toFileName(String name) {
		if (name == null) return null;
		return "data/" + name.split("\\.")[0] + ".map";
	}


	public float localize(int p) {
		return BOUND + HALF + p * BOUND;
	}


	public PVector localize(Grid g) {
		return new PVector(localize(g.x), localize(g.y));
	}


	public void openfile(String file) {
		try {
			clear();
			Scanner in = new Scanner(new File(file));
			while (in.hasNextInt()) {
				int x = in.nextInt();
				int y = in.nextInt();
				nodes[y][x].valid = false;
			}
			in.close();
		} catch (Exception e) {}
		invalidate();
	}


	public void write(String file) {
		try {
			PrintStream writer = new PrintStream(new FileOutputStream(file));
			for (int y = 0; y < nodes.length; y++)
				for (int x = 0; x < nodes[0].length; x++)
					if (!nodes[y][x].valid)
						writer.println(x + " " + y);
			writer.close();
		} catch (Exception e) {
		}
	}


	private final float RECT_POINTS[][] = {
			{-HALF, -HALF, -HALF, HALF},
			{-HALF, -HALF, HALF, -HALF},
			{-HALF, HALF, HALF, HALF},
			{HALF, -HALF, HALF, HALF},
	};


	public boolean rayCast(PVector dest, PVector position) {
		HashSet<Node> check = getRegion(dest, position);
		if (check == null) return false;

		for (Node n : check) {
			for (float[] i : RECT_POINTS) {
				PVector intersection = Rays.segmentIntersectionPoint(
						position.x, position.y, dest.x, dest.y,
						n.p.x + i[0], n.p.y + i[1], n.p.x + i[2], n.p.y + i[3]);

				if (intersection == null) {
					float dx = position.x - dest.x;
					float dy = position.y - dest.y;
					PVector normal = new PVector(-dy, dx);
					normal.normalize();
					normal.mult(Boid.DEFAULT_SIZE);

					//Normals to avoid corner cutting
					PVector intersection1 = Rays.segmentIntersectionPoint(
							position.x + normal.x, position.y + normal.y, dest.x + normal.x, dest.y + normal.y,
							n.p.x + i[0], n.p.y + i[1], n.p.x + i[2], n.p.y + i[3]);
					PVector intersection2 = Rays.segmentIntersectionPoint(
							position.x - normal.x, position.y - normal.y, dest.x - normal.x, dest.y - normal.y,
							n.p.x + i[0], n.p.y + i[1], n.p.x + i[2], n.p.y + i[3]);

					if (intersection1 == null && intersection2 == null) continue;		
				}

				return false;
			}
		}
		return true;
	}


	private HashSet<Node> getRegion(PVector dest, PVector position) {
		Grid b = quantize(position.x, position.y);
		Grid g = quantize(dest.x, dest.y);
		if (b == null | g == null) return null;

		HashSet<Node> check = new HashSet<>();

		int deltax = b.x - g.x;
		int deltay = b.y - g.y;
		float error = 0;

		if (deltax == 0) {
			for (int y = Math.min(b.y, g.y); y <= Math.max(b.y, g.y); y++) {
				group(check, b.x, y);
			}
		} else {
			float deltaerr = Math.abs((float) deltay / deltax);
			int y = b.y;
			int x;
			for (x = b.x; b.x < g.x ? x <= g.x : x >= g.x; x += Math.signum(g.x - b.x)) {
				group(check, x, y);
				error += deltaerr;
				while (error >= 0.5f) {
					group(check, x, y);
					y += Math.signum(g.y - b.y);
					error -= 1;
				}
			}
		}
		return check;
	}


	private void group(HashSet<Node> check, int xx, int yy) {
		for (int x = xx - 1; x <= xx + 1; x++)
			for (int y = yy - 1; y <= yy + 1; y++)
				if (x >= 0 && x < nodes[0].length && y >= 0 && y < nodes.length && !nodes[y][x].valid)
					check.add(nodes[y][x]);
	}


	public Node nodify(PVector p) {
		return nodify(p.x, p.y);
	}


	public Node nodify(float x, float y) {
		Grid grid = quantize(x, y);
		if (grid == null) return null;
		return nodes[grid.y][grid.x];
	}


	public Node nodify(Grid grid) {
		return nodes[grid.y][grid.x];
	}


	public void navigate(Navigator boid) {
		boid.target(boid.position());
		if (boid.goal() == null) return;
		Algorithm alg = boid.alg();
		if (!valid || (alg == null && boid.goal() != null) ||
				(alg != null && nodify(boid.goal()).i != alg.getGoal().i))
			alg = solve(boid);

		if (alg == null || !alg.getSolved()) return;
		PVector target = alg.getPath()[0].p;
		
		if (alg != null && alg.getSolved()) {
			Node[] path = alg.getPath();
			PVector goal = localize(boid.goal());
			if (rayCast(boid.position(), goal))
				target = goal;
			else {
				for (int i = path.length - 1; i >= 0; i--) {
					if (rayCast(boid.position(), path[i].p)) {
						target = path[i].p.get();
						break;
					}
				}
			}
		}
		boid.target(target);
	}


	public void adjust(int x, int y, boolean shifting) {
		Node node = nodify(x, y);
		if (node == null) return;
		if (node.valid == shifting) return;
		node.valid = shifting;
		invalidate();
	}


	public void update() {
		valid = true;
		for (WorldEvent event : worldEvents)
			event.update();
	}


	private void invalidate() {
		valid = false;
		setEdges();
	}


	public void clear() {
		for (Node[] y : nodes)
			for (Node x : y)
				x.valid = true;
		invalidate();
	}

	
	public void collision(Entity entity) {
		for (float i = 1; ; i += 0.01f) {
			PVector tmp = entity.position().get();
			Node hit = nodify(tmp);
			if (hit == null) {
				entity.setPosition(openPosition());
				return;
			}
			
			if (hit.valid) return;
			hit.collide = true;
			tmp = PVector.sub(tmp, hit.p);
			if (tmp.magSq() == 0)
				tmp = new PVector(1, 1);
			tmp.setMag(HALF * i + entity.size());
			tmp.add(hit.p);
			entity.position().x = tmp.x;
			entity.position().y = tmp.y;
		}
	}

	public Random random = new Random();
	public PVector openPosition() {
		Node node = null;
		for (int i = 0; i < 5000; i++) {
			Node[] row = nodes[random.nextInt(nodes.length)];
			node = row[random.nextInt(row.length)];
			if (node.valid) return node.p.get();
		}
		node.valid = true;
		invalidate();
		return node.p.get();
	}
}
