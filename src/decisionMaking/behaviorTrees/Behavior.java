package decisionMaking.behaviorTrees;

import parameterization.AvailableActions;
import parameterization.Parameterization;
import decisionMaking.TreeNode;

public class Behavior implements TreeNode {

	String action, successCondition, failCondition;
	boolean successState = true;
	boolean failState = true;
	
	public Behavior(String action, String successCondition) {
		this(action, successCondition, null);
	}
	
	
	public Behavior(String action, String successCondition, String failCondition,
			boolean reverseSuccess, boolean reverseFailure) {
		this.action = action;
		this.successCondition = successCondition;
		this.failCondition = failCondition;
		successState = !reverseSuccess;
		failState = !reverseFailure;
	}
	
	
	public Behavior(String action, String successCondition, String failCondition) {
		this.action = action;
		this.successCondition = successCondition;
		this.failCondition = failCondition;
	}
	
	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		if (DEBUG_BEHAVIORS) System.out.println(action);
		a.act(action);
		if (p.get(successCondition) == successState) return TRUE;
		if (failCondition != null && p.get(failCondition) == failState) return FALSE;
		return ONGOING;
	}


	@Override
	public String toString(int l) {
		String build = TreeNode.ident(l);
		build += "Behavior: " + action;
		build += " / sucess=" + (successState ? "" : "!") + successCondition;
		if (failCondition != null)
			build += " / failure=" + (failState ? "" : "!") + failCondition;
		build += '\n';
		return build;
	}
}
