package entities;

import behaviors.BehaviorTemplate;
import processing.core.PApplet;

public class Bomb extends Entity {

	public static final int DEFAULT_SIZE = 20;
	
	public Bomb(float x, float y, BehaviorTemplate... behaviors) {
		this(x, y, DEFAULT_SIZE, behaviors);
	}	
	
	public Bomb(float x, float y, float s,
			BehaviorTemplate... behaviors) {
		super(x, y, s, 1, 0, behaviors);
	}

	@Override
	public void drawCrumb(PApplet g, float x, float y, float r) {}

	@Override
	public void draw(PApplet g) {
		g.noStroke();
		g.fill(0xffffffff);
		g.rect(position.x - size / 8, position.y, size / 4, -size);
		g.fill(percent * 255, 0, 0);
		g.ellipse(position.x, position.y, size, size);
	}
	
	
}
