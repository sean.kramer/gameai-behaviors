package parameterization;

import processing.core.PVector;
import util.Engine;
import util.Grid;
import entities.Bomb;
import entities.Coin;
import entities.Explosion;

public class PlayerActions extends AvailableActions {
	
	public boolean act(String action) {
		switch (action) {
		
		case SEEK_COIN:
			PVector nearest = Engine.instance().nearest(Coin.class, entity).position();
			entity.goal(Engine.world().quantize(nearest));
			break;

		case WANDER:
			entity.wander();
			break;
			
		case IDLE:
			entity.idle();
			break;
			
		case SEEK_BOMB:
			Grid goal = Engine.world().quantize(Engine.instance().nearest(Bomb.class, entity).position());
			entity.goal(goal);
			break;
			
		case FLEE_BOMB:
			goal = Engine.instance().safeFrom(Explosion.class, entity, Explosion.DEFAULT_BLAST * 0.7f);
			entity.goal(goal);
		}
		
		return true;
	}
	
}
