package decisionMaking.behaviorTrees;

import parameterization.AvailableActions;
import parameterization.Parameterization;

public class Selector extends Picker {

	@Override
	public int evaluate(Parameterization p, AvailableActions a, int l) {
		int result = children.get(state).evaluate(p, a, l + 1);
		while (result == FALSE) {
			state++;
			if (state == children.size()) {
				state = 0;
				return FALSE;
			}
			result = children.get(state).evaluate(p, a, l + 1);
		}
		if (result == TRUE)
			state = 0;
		return result;
	}

}
