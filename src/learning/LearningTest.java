package learning;

import java.util.HashMap;

public class LearningTest {

	static String[] att = {"health", "cover", "ammo"};
	
	static String[] act = {"attack","attack","defend","defend","defend"};
	
	static boolean[][] test = {
			{true, true, true},
			{false, true, true},
			{true, true, false},
			{false, true, false},
			{false, false, true},
	};

	public static void main(String[] args) {
		MakeTree tree = new MakeTree(null, 3, "health","cover","ammo","attack","defend");
		
		for (int i = 0; i < act.length; i++) {
			HashMap<String, Boolean> map = new HashMap<String, Boolean>();
			for (int j = 0; j < test[i].length; j++) {
				map.put(att[j], test[i][j]);
			}
			tree.inform(act[i], map);
		}
		
		System.out.println(tree.generate().toString(0));
	}
	
}
