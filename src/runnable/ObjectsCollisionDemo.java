package runnable;

import processing.core.PGraphics;
import processing.event.KeyEvent;
import processing.event.MouseEvent;
import util.Simulation;
import world.World;
import behaviors.Explode;
import entities.Bomb;
import entities.Coin;

public class ObjectsCollisionDemo extends Simulation {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;

	private World world;
	
	public ObjectsCollisionDemo() {
		super();
		
		Coin coin = new Coin(300, 200);
		Bomb bomb = new Bomb(600, 200);
		Bomb bomb2 = new Bomb(600, 400, new Explode(100));
		Bomb bomb3 = new Bomb(300, 400, new Explode(140));
		
		engine.addEntity(coin);
		engine.addEntity(bomb);
		engine.addEntity(bomb2);
		engine.addEntity(bomb3);
		engine.begin();
	}



	@Override
	public void mousePressed(MouseEvent e) {
		world.adjust(e.getX(), e.getY(), shifting);
	}


	private boolean shifting = false;

	
	@Override
	public void keyReleased(KeyEvent e) {
		shifting = e.isShiftDown();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		world.adjust(e.getX(), e.getY(), shifting);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		shifting = e.isShiftDown();
		switch (e.getKey()) {
		case 'r':
			world.clear();
			break;
		default:
			//int i = e.getKey() - '1';
			//TODO terrain types
		}
	}
	@Override
	public String getName() {
		return "Objects and Collision Demo";
	}


	public static void main(String[] args) {new ObjectsCollisionDemo();}


	@Override
	public World getWorld() {
		this.world = new World();
		return world;
	}


	@Override
	public void draw(PGraphics g) {
		// TODO Auto-generated method stub
		
	}
}
