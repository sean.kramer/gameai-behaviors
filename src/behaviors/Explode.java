package behaviors;

import processing.core.PApplet;
import util.Engine;
import entities.Entity;
import entities.Explosion;

public class Explode extends BehaviorTemplate {
	
	private int timer;
	private float max;
	private Explosion explosion = null;
	
	public Explode(int frames) {
		timer = frames;
		max = frames;
	}
	
	
	@Override
	public void apply(Entity boid, PApplet g) {
		if (explosion == null && timer * 2 < max) {
			explosion = new Explosion(boid.position().x, boid.position().y);
			Engine.instance().addEntity(explosion);
		}
		timer--;
		if (timer == 0) {
			explosion.activate();
			Engine.instance().removeEntity(boid);
		}
			
		boid.percent(1 - timer / max);
	}
}
